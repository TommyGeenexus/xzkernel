From 7bfb3c7263f55c93055059e98364686d33a7dae8 Mon Sep 17 00:00:00 2001
From: Paul Reioux <reioux@gmail.com>
Date: Wed, 24 Jul 2013 19:18:35 -0500
Subject: [PATCH] Low CPU Clocks: Allow ultra low CPU frequencies for better
 battery performance

while screen is on or background operations like audio books and music

Signed-off-by: Paul Reioux <reioux@gmail.com>
---
 arch/arm/mach-msm/Kconfig           |  6 ++++
 arch/arm/mach-msm/acpuclock-8064.c  | 56 +++++++++++++++++++++++++++++++++++++
 arch/arm/mach-msm/acpuclock-krait.c |  8 ++++++
 3 files changed, 70 insertions(+)

diff --git a/arch/arm/mach-msm/Kconfig b/arch/arm/mach-msm/Kconfig
index 651ed74..448c822 100644
--- a/arch/arm/mach-msm/Kconfig
+++ b/arch/arm/mach-msm/Kconfig
@@ -1629,6 +1629,12 @@ config MSM_CPU_FREQ_MIN
 
 endif # CPU_FREQ_MSM
 
+config LOW_CPUCLOCKS
+	bool "Enable ultra low CPU clocks"
+	default n
+	help
+	  Ultra low cpu frequencies enabled for CPU and L2 Cache
+
 config INTELLI_PLUG
 	bool "Enable intelli-plug cpu hotplug driver"
 	default n
diff --git a/arch/arm/mach-msm/acpuclock-8064.c b/arch/arm/mach-msm/acpuclock-8064.c
index 472619b..34d1318 100644
--- a/arch/arm/mach-msm/acpuclock-8064.c
+++ b/arch/arm/mach-msm/acpuclock-8064.c
@@ -132,6 +132,25 @@
 };
 
 static struct l2_level l2_freq_tbl[] __initdata = {
+#ifdef CONFIG_LOW_CPUCLOCKS
+	[0]  = { {  378000, HFPLL, 2, 0x1C },  950000, 1050000, 1 },
+	[1]  = { {  384000, PLL_8, 0, 0x00 },  950000, 1050000, 1 },
+	[2]  = { {  432000, HFPLL, 2, 0x20 }, 1050000, 1050000, 2 },
+	[3]  = { {  486000, HFPLL, 2, 0x24 }, 1050000, 1050000, 2 },
+	[4]  = { {  540000, HFPLL, 2, 0x28 }, 1050000, 1050000, 2 },
+	[5]  = { {  594000, HFPLL, 1, 0x16 }, 1050000, 1050000, 2 },
+	[6]  = { {  648000, HFPLL, 1, 0x18 }, 1050000, 1050000, 4 },
+	[7]  = { {  702000, HFPLL, 1, 0x1A }, 1050000, 1050000, 4 },
+	[8]  = { {  756000, HFPLL, 1, 0x1C }, 1150000, 1150000, 4 },
+	[9]  = { {  810000, HFPLL, 1, 0x1E }, 1150000, 1150000, 4 },
+	[10] = { {  864000, HFPLL, 1, 0x20 }, 1150000, 1150000, 4 },
+	[11] = { {  918000, HFPLL, 1, 0x22 }, 1150000, 1150000, 5 },
+	[12] = { {  972000, HFPLL, 1, 0x24 }, 1150000, 1150000, 5 },
+	[13] = { { 1026000, HFPLL, 1, 0x26 }, 1150000, 1150000, 5 },
+	[14] = { { 1080000, HFPLL, 1, 0x28 }, 1150000, 1150000, 5 },
+	[15] = { { 1134000, HFPLL, 1, 0x2A }, 1150000, 1150000, 5 },
+	[16] = { { 1188000, HFPLL, 1, 0x2C }, 1150000, 1150000, 5 },
+#else
 	[0]  = { {  384000, PLL_8, 0, 0x00 },  950000, 1050000, 1 },
 	[1]  = { {  432000, HFPLL, 2, 0x20 }, 1050000, 1050000, 2 },
 	[2]  = { {  486000, HFPLL, 2, 0x24 }, 1050000, 1050000, 2 },
@@ -148,11 +167,21 @@
 	[13] = { { 1080000, HFPLL, 1, 0x28 }, 1150000, 1150000, 5 },
 	[14] = { { 1134000, HFPLL, 1, 0x2A }, 1150000, 1150000, 5 },
 	[15] = { { 1188000, HFPLL, 1, 0x2C }, 1150000, 1150000, 5 },
+#endif
 	{ }
 };
 
 static struct acpu_level tbl_slow[] __initdata = {
+#ifdef CONFIG_LOW_CPUCLOCKS
+	{ 1, {   162000, HFPLL, 2, 0x0C }, L2(0),   900000 },
+	{ 1, {   216000, HFPLL, 2, 0x10 }, L2(0),   900000 },
+	{ 1, {   270000, HFPLL, 2, 0x14 }, L2(0),   925000 },
+	{ 1, {   324000, HFPLL, 2, 0x18 }, L2(0),   925000 },
+	{ 1, {   378000, HFPLL, 2, 0x1C }, L2(0),   950000 },
+	{ 1, {   384000, PLL_8, 0, 0x00 }, L2(1),   950000 },
+#else
 	{ 1, {   384000, PLL_8, 0, 0x00 }, L2(0),   950000 },
+#endif
 	{ 0, {   432000, HFPLL, 2, 0x20 }, L2(5),   975000 },
 	{ 1, {   486000, HFPLL, 2, 0x24 }, L2(5),   975000 },
 	{ 0, {   540000, HFPLL, 2, 0x28 }, L2(5),  1000000 },
@@ -194,7 +223,16 @@
 };
 
 static struct acpu_level tbl_nom[] __initdata = {
+#ifdef CONFIG_LOW_CPUCLOCKS
+	{ 1, {   162000, HFPLL, 2, 0x0C }, L2(0),   850000 },
+	{ 1, {   216000, HFPLL, 2, 0x10 }, L2(0),   850000 },
+	{ 1, {   270000, HFPLL, 2, 0x14 }, L2(0),   875000 },
+	{ 1, {   324000, HFPLL, 2, 0x18 }, L2(0),   875000 },
+	{ 1, {   378000, HFPLL, 2, 0x1C }, L2(0),   900000 },
+	{ 1, {   384000, PLL_8, 0, 0x00 }, L2(1),   900000 },
+#else
 	{ 1, {   384000, PLL_8, 0, 0x00 }, L2(0),   900000 },
+#endif
 	{ 0, {   432000, HFPLL, 2, 0x20 }, L2(5),   925000 },
 	{ 1, {   486000, HFPLL, 2, 0x24 }, L2(5),   925000 },
 	{ 0, {   540000, HFPLL, 2, 0x28 }, L2(5),   950000 },
@@ -236,7 +274,16 @@
 };
 
 static struct acpu_level tbl_fast[] __initdata = {
+#ifdef CONFIG_LOW_CPUCLOCKS
+	{ 1, {   162000, HFPLL, 2, 0x0C }, L2(0),   800000 },
+	{ 1, {   216000, HFPLL, 2, 0x10 }, L2(0),   800000 },
+	{ 1, {   270000, HFPLL, 2, 0x14 }, L2(0),   825000 },
+	{ 1, {   324000, HFPLL, 2, 0x18 }, L2(0),   825000 },
+	{ 1, {   378000, HFPLL, 2, 0x1C }, L2(0),   850000 },
+	{ 1, {   384000, PLL_8, 0, 0x00 }, L2(1),   850000 },
+#else
 	{ 1, {   384000, PLL_8, 0, 0x00 }, L2(0),   850000 },
+#endif
 	{ 0, {   432000, HFPLL, 2, 0x20 }, L2(5),   875000 },
 	{ 1, {   486000, HFPLL, 2, 0x24 }, L2(5),   875000 },
 	{ 0, {   540000, HFPLL, 2, 0x28 }, L2(5),   900000 },
@@ -278,7 +325,16 @@
 };
 
 static struct acpu_level tbl_faster[] __initdata = {
+#ifdef CONFIG_LOW_CPUCLOCKS
+	{ 1, {   162000, HFPLL, 2, 0x0C }, L2(0),   800000 },
+	{ 1, {   216000, HFPLL, 2, 0x10 }, L2(0),   800000 },
+	{ 1, {   270000, HFPLL, 2, 0x14 }, L2(0),   825000 },
+	{ 1, {   324000, HFPLL, 2, 0x18 }, L2(0),   825000 },
+	{ 1, {   378000, HFPLL, 2, 0x1C }, L2(0),   850000 },
+	{ 1, {   384000, PLL_8, 0, 0x00 }, L2(1),   850000 },
+#else
 	{ 1, {   384000, PLL_8, 0, 0x00 }, L2(0),   850000 },
+#endif
 	{ 0, {   432000, HFPLL, 2, 0x20 }, L2(5),   875000 },
 	{ 1, {   486000, HFPLL, 2, 0x24 }, L2(5),   875000 },
 	{ 0, {   540000, HFPLL, 2, 0x28 }, L2(5),   900000 },
diff --git a/arch/arm/mach-msm/acpuclock-krait.c b/arch/arm/mach-msm/acpuclock-krait.c
index e1d8347..be4c5db 100644
--- a/arch/arm/mach-msm/acpuclock-krait.c
+++ b/arch/arm/mach-msm/acpuclock-krait.c
@@ -47,10 +47,18 @@
 #define SECCLKAGD		BIT(4)
 
 #ifdef CONFIG_OC_ULTIMATE
+#ifdef CONFIG_LOW_CPUCLOCKS
+#define FREQ_TABLE_SIZE		41
+#else
 #define FREQ_TABLE_SIZE		37
+#endif
+#else
+#ifdef CONFIG_LOW_CPUCLOCKS
+#define FREQ_TABLE_SIZE		39
 #else
 #define FREQ_TABLE_SIZE		35
 #endif
+#endif
 
 static DEFINE_MUTEX(driver_lock);
 static DEFINE_SPINLOCK(l2_lock);
-- 
1.8.1.6

